{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-20.09";
    boijpkgs.url = "gitlab:shanshui/boij";
  };

  outputs = { self, nixpkgs, boijpkgs }: {
    nixosConfigurations.shan-shui-dortmund = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        ./shan-shui/dortmund.nix
        { nixpkgs.overlays = [ boijpkgs.overlay ]; }
      ];
    };
    nixosConfigurations.shan-shui-usb = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        ./shan-shui/usb.nix
        { nixpkgs.overlays = [ boijpkgs.overlay ]; }
      ];
    };
  };
}
