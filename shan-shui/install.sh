#!/bin/sh -x

dev=$1
p=$2

if [ ! -e "$dev" ]; then
    echo "usage: $0 [disk] [p]"
    exit 1
fi

nix-env -iA nixos.nixFlakes nixos.git || exit 1

parted $dev -- mklabel gpt || exit 1
parted $dev -- mkpart primary 512MiB 100% || exit 1
parted $dev -- mkpart ESP fat32 1MiB 512MiB || exit 1
parted $dev -- set 2 esp on || exit 1

mkfs.ext4 -L nixos $dev${p}1 || exit 1
mkfs.fat -F 32 -n boot $dev${p}2 || exit 1

mount /dev/disk/by-label/nixos /mnt || exit 1
mkdir -p /mnt/boot /mnt/etc || exit 1
mount /dev/disk/by-label/boot /mnt/boot || exit 1
cd /mnt/etc || exit 1
git clone https://gitlab.com/shanshui/nixos.git/ || exit 1
cd nixos || exit 1
git config pull.rebase false || exit 1
