{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ./configuration.nix
    ];

  time.timeZone = "Europe/Amsterdam";

  networking.hostName = "shan-shui-usb";

  networking.interfaces.eth0 = {
    useDHCP = true;
    ipv4.addresses = [
      { address = "192.168.0.222"; prefixLength = 24; }
    ];
  };

  networking.nameservers = [
    "1.1.1.1"
    "9.9.9.9"
  ];

  # Enable wireless support via wpa_supplicant.
  #networking.wireless.enable = true;
  #networking.interfaces.wlan0.useDHCP = true;
}
