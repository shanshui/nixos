{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ./configuration.nix
    ];

  time.timeZone = "Europe/Berlin";

  networking.hostName = "shan-shui-dortmund";

  networking.interfaces.eth0 = {
    useDHCP = false;
    ipv4.addresses = [
      { address = "192.168.0.222"; prefixLength = 24; }
      { address = "31.172.2.145"; prefixLength = 31; }
    ];
  };

  networking.defaultGateway = "31.172.2.144";

  networking.nameservers = [
    "195.182.2.2"
    "195.182.2.22"
    "1.1.1.1"
    "9.9.9.9"
  ];

  # Enable wireless support via wpa_supplicant.
  #networking.wireless.enable = true;
  #networking.interfaces.wlan0.useDHCP = true;
}
