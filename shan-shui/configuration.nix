# Edit this configuration file to define what should be installed on your system.
# Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.kernelParams = [ "net.ifnames=0" ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  powerManagement.cpuFreqGovernor = "performance";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;

  networking.hosts = {
    "192.168.0.1" = [ "sick" ];
  };

  # Open ports in the firewall.
  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 55022 55122 55222 ];
    allowedUDPPorts = [ ];
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.boij = {
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" "video" "render" "input" ];
  };

  security.sudo.extraRules = [
    {
      groups = [ "wheel" ];
      commands = [
        { command = "ALL"; options = [ "NOPASSWD" ]; }
      ];
    }
  ];

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Configure keymap in X11
  services.xserver.layout = "us";
  #services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support (enabled default in most desktopManager).
  #services.xserver.libinput.enable = true;

  services.xserver.displayManager = {
    autoLogin = {
        enable = true;
        user = "boij";
    };

    sddm = {
      enable = true;
      autoLogin.relogin = true;
    };

    session = [ {
      manage = "desktop";
      name = "boij";
      start = ''
        ${pkgs.xorg.xsetroot}/bin/xsetroot -solid black
        ${pkgs.xorg.xset}/bin/xset -dpms
        ${pkgs.boij}/bin/sick-runner.sh &
        waitPID=$!
      '';
    } ];
  };

  systemd.services.display-manager.after = [ "acpid.service" "systemd-logind.service" "systemd-user-sessions.service" ];

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    ports = [ 55022 55122 55222 ];
    passwordAuthentication = false;
    challengeResponseAuthentication = false;
  };

  services.tor = {
    enable = false;
    hiddenServices.ssh = {
      map = [ { port = 55022; } ];
      version = 3;
    };
    extraConfig = ''
      HiddenServiceNonAnonymousMode 1
      HiddenServiceSingleHopMode 1
      HiddenServiceNumIntroductionPoints 20
    '';
  };

  services.autossh.sessions = [
    {
      name = "tunnel";
      user = "boij";
      extraArguments = "-N -o 'ServerAliveInterval 20' -o 'ServerAliveCountMax 3' tunnel";
    }
  ];

  systemd.coredump.enable = true;
  systemd.coredump.extraConfig = "Storage=none";

  # Enable ALSA sound.
  sound.enable = true;

  # Enable nixFlakes by default:
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  # List packages installed in system profile. To search, run:
  # $ nix search nixpkgs wget
  environment.systemPackages = with pkgs; [
     tmux htop
     mg zile calc
     wget curl
     nmap socat host tcpdump tcpflow
     #wireshark-cli
     #tcpreplay
     iproute inetutils wpa_supplicant
     utillinux pmutils
     alsaUtils sox
     imagemagick
     git sqlite
     gdb glxinfo
     x11vnc scrot ffmpeg mpv
     boij
     (pkgs.writeScriptBin "system-update" ''
      #!/usr/bin/env bash
      cd /etc/nixos
      git pull
      nixos-rebuild switch
     '')
  ];

  #nixpkgs.config.permittedInsecurePackages = [
  #  "tcpreplay-4.3.3"
  #];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?
}
